package net.nilosplace.Antz.Map;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.nilosplace.Antz.AntzHandler;
import net.nilosplace.Antz.AntMoveResponce;
import net.nilosplace.Antz.Ants.Ant;
import net.nilosplace.Antz.Ants.AntMove;
import net.nilosplace.Antz.Ants.Direction;


public class Mapper {
	
	private RandomAccessFile mapFile;
	private int mapSize = 1000000;
	private volatile long map[] = new long[mapSize];
	private String fileName;
	private NestObject nest = null;
	
	public Mapper(String fileName) {
		this.fileName = fileName;
		loadMap();
		//initMap();
		//saveMap();
	}
	
	private void loadMap() {
		try {
			if(mapFile == null) mapFile = new RandomAccessFile(fileName, "rw");
			mapFile.length();
			Date start = new Date();
			System.out.println("Loading map into memory");
			
			
			byte[] temp = new byte[mapSize * 8];
			mapFile.read(temp);
			for(int i = 0; i < mapSize; i++) {

				map[i] =
					((0xFF & (long)temp[(i * 8) + 0]) << 56) +
					((0xFF & (long)temp[(i * 8) + 1]) << 48) +
					((0xFF & (long)temp[(i * 8) + 2]) << 40) +
					((0xFF & (long)temp[(i * 8) + 3]) << 32) +
					((0xFF & (long)temp[(i * 8) + 4]) << 24) +
					((0xFF & (long)temp[(i * 8) + 5]) << 16) +
					((0xFF & (long)temp[(i * 8) + 6]) << 8) +
					((0xFF & (long)temp[(i * 8) + 7]));
				//MapObject m = createMapObject(i, map[i]);
				/*
				 if(m.getMapObjectType() != MapObject.MAP_TYPE_GROUND) {
					System.out.print("FileData: ");
					System.out.print(Integer.toBinaryString(temp[(i * 8) + 0]) + ":");
					System.out.print(Integer.toBinaryString(temp[(i * 8) + 1]) + ":");
					System.out.print(Integer.toBinaryString(temp[(i * 8) + 2]) + ":");
					System.out.print(Integer.toBinaryString(temp[(i * 8) + 3]) + ":");
					System.out.print(Integer.toBinaryString(temp[(i * 8) + 4]) + ":");
					System.out.print(Integer.toBinaryString(temp[(i * 8) + 5]) + ":");
					System.out.print(Integer.toBinaryString(temp[(i * 8) + 6]) + ":");
					System.out.println(Integer.toBinaryString(temp[(i * 8) + 7]));
					m.print();
				}
				*/

			}
			System.out.println("Temp Finished");
			
			/*
			for(int i = 0; i < mapSize; i++) {
				//mapFile.read
				mapFile.seek(i * 8);
				map[i] = mapFile.readLong();
				//if(map[i] != 3) {
				//	System.out.println("Reading Data: " + Long.toBinaryString(map[i]));
				//	getMapObject(i).print();
				//}
			}
			*/
			
			mapFile.close();
			mapFile = null;
			System.out.println("Finished Loading Map into memory");
			Date end = new Date();
			System.out.println("Load took: " + (end.getTime() - start.getTime()) + " seconds");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initMap() {
		long data;
		for(int i = 0; i < mapSize; i++) {
			if(i == 200200) {
				// Nest with 1000 ants 200000 storage
				NestObject n = new NestObject(i);
				n.addToFoodStored(100000);
				n.setTotalAntz(1000);
				data = n.getNest();
				map[i] = data;
			} /* else if(i == 200400) {
				GroundObject g = new GroundObject(i);
				g.setAntEnergyAmount(1023);
				g.setAntWeightCarried(1023);
				g.setAtype(MapObject.ANT_TYPE_FORAGER);
				g.setHasAnt(true);
				g.setPheromoneAmount(0);
				g.setPtype(MapObject.PHEROMONE_TYPE_NONE);
				map[i] = g.getGround();
			} */ else {
				// Ground with no mones
				GroundObject g = new GroundObject(i);
				map[i] = g.getGround();
			}
		}
	}
	
	public void saveMap() {
		synchronized (this) {
			try {
				if(mapFile == null) mapFile = new RandomAccessFile(fileName, "rw");
				System.out.println("Saving Map from memory to file: " + fileName);
				for(int i = 0; i < mapSize; i++) {
					mapFile.seek(i * 8);
					mapFile.writeLong(map[i]);
				}
				mapFile.close();
				mapFile = null;
				System.out.println("Finished Saving Map from memory to file: " + fileName);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public MapObject getMapObject(int id) {
		//System.out.println("getMapObject: id: " + id);
		return createMapObject(id, map[id]);
	}
	
	public MapObject getMapObject(int x, int y) {
		//System.out.println("getMapObject: x: " + x + " y: " + y);
		if(x >= 1000 || y >= 1000 || x < 0 || y < 0) return null;
		int id = (y * 1000) + x;
		return createMapObject(id, map[id]);
	}
	

	public List<Ant> getInitialAnts(AntzHandler antzHandler) {
		//System.out.println("getInitialAnts: ");
		List<Ant> antz = new ArrayList<Ant>();
		System.out.println("Loading Antz");
		for(int i = 0; i < mapSize; i++) {
			MapObject m = createMapObject(i, map[i]);
			if(m.getMapObjectType() == MapObject.MAP_TYPE_GROUND) {
				GroundObject g = (GroundObject)m;
				if(g.isHasAnt()) {
					antz.add(new Ant(i, antzHandler, g));
				}
			}
		}
		System.out.println("Finished Loading Antz");
		return antz;
	}
	
	//public boolean saveMapObject(int id, MapObject m) {
		
	//	return true;
	//}
	
	private MapObject createMapObject(int id, long data) {
		//System.out.println("createMapObject id: " + id + " Data: " + Long.toBinaryString(data));
		MapObject o;
		long type = data & 3;
		switch ((int)type) {
			case(MapObject.MAP_TYPE_BLOCKAGE):
				o = new BlockageObject(id, data);
				break;
			case(MapObject.MAP_TYPE_FOOD):
				o = new FoodObject(id, data);
				break;
			case(MapObject.MAP_TYPE_NEST):
				o = new NestObject(id, data);
				nest = (NestObject)o;
				break;
			case(MapObject.MAP_TYPE_GROUND):
				o = new GroundObject(id, data);
				break;
			default:
				o = null;
		}
		//o.print();
		return o;
	}

	private AntMoveResponce moveGround(GroundObject mfrom, GroundObject mto, int pheromoneType) throws AntException {
		//System.out.println("moveGround: ");
		if(mfrom.getAntEnergyAmount() == 1) {
			mfrom.clearAntData();
			map[mfrom.getId()] = mfrom.getData();
			throw new AntException(AntException.DIED);
		} else {
			mfrom.useEnergy(pheromoneType);
			mto.setAntData(mfrom);
			mfrom.clearAntData();
			map[mfrom.getId()] = mfrom.getData();
			map[mto.getId()] = mto.getData();
			AntMoveResponce res = new AntMoveResponce(mto);
			return res;
		}
	}
	
	private AntMoveResponce moveFood(GroundObject mfrom, FoodObject mto) {
		System.out.println("moveFood: ");
		mto.takeFood(mfrom);
		mfrom.updateData();
		mto.updateData();
		map[mfrom.getId()] = mfrom.getData();
		map[mto.getId()] = mto.getData();
		if(mto.getFoodAmount() == 0) {
			GroundObject g = new GroundObject(mto.getId());
			map[mto.getId()] = g.getData();
		} else {
			map[mto.getId()] = mto.getData();
		}
		AntMoveResponce res = new AntMoveResponce(mfrom);
		return res;
	}
	
	private AntMoveResponce moveNest(GroundObject mfrom, NestObject mto, int pheromoneType) throws AntException {
		System.out.println("moveNest: ");
		mfrom.useEnergy(pheromoneType);
		mfrom.transferFood(mto);
		mfrom.updateData();
		mto.updateData();
		map[mfrom.getId()] = mfrom.getData();
		map[mto.getId()] = mto.getData();
		nest = mto;
		AntMoveResponce res = new AntMoveResponce(mfrom);
		System.out.println("moveNest finished: ");
		return res;
	}
	
	public AntMoveResponce moveAnt(AntMove move) throws AntException {
		synchronized (this) {
			int id = move.getId();
			Direction d = move.getDirection();
			int pheromoneType = move.getPheromoneType();
			//System.out.println("moveAnt: " + id + " Dir: " + d);
			MapObject mfrom = getMapObject(id);
			Surrounding s = getSurrounding(id, d);
			MapObject mto = s.getMapObject();
			if(mto == null || mto.getMapObjectType() == MapObject.MAP_TYPE_BLOCKAGE) {
				throw new AntException(AntException.BLOCKED);
			}
			//mto.print();
			switch(mto.getMapObjectType()) {
				case(MapObject.MAP_TYPE_GROUND):
					GroundObject g = (GroundObject)mto;
					if(g.isHasAnt()) throw new AntException(AntException.ANTBLOCKED);
					else return moveGround((GroundObject)mfrom, (GroundObject)mto, pheromoneType);
				case(MapObject.MAP_TYPE_FOOD):
					return moveFood((GroundObject)mfrom, (FoodObject)mto);
				case(MapObject.MAP_TYPE_NEST):
					return moveNest((GroundObject)mfrom, (NestObject)mto, pheromoneType);
			}
			throw new AntException(AntException.UNKNOWN);
		}
	}
	
	public MapObject getObject(int x, int y) {
		synchronized (this) {
			return getMapObject(x, y);
		}
	}

	public List<Surrounding> getSurroundings(int id, boolean ordered) {
		synchronized (this) {
			List<Surrounding> surs = new ArrayList<Surrounding>();
			List<Surrounding> ret = new ArrayList<Surrounding>();
			
			surs.add(getSurrounding(id, Direction.EAST));
			surs.add(getSurrounding(id, Direction.WEST));
			surs.add(getSurrounding(id, Direction.NORTH));
			surs.add(getSurrounding(id, Direction.SOUTH));
			surs.add(getSurrounding(id, Direction.NORTHEAST));
			surs.add(getSurrounding(id, Direction.NORTHWEST));
			surs.add(getSurrounding(id, Direction.SOUTHEAST));
			surs.add(getSurrounding(id, Direction.SOUTHWEST));
			
			if(ordered) {
				while(surs.size() > 0) {
					long maxmones = 0;
					int count = 0;
					int index = 0;
					for(Surrounding s: surs) {
						if(s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_GROUND) {
							long mones = ((GroundObject)s.getMapObject()).getPheromoneAmount();
							if(mones > maxmones) {
								maxmones = mones;
								index = count;
							}
						}
						count++;
					}
					ret.add(surs.remove(index));
				}
				return ret;
			} else {
				return surs;
			}
		}
	}
	
	public Surrounding getSurrounding(int id, Direction d) {
		int y = (int)(id / 1000);
		int x = id - (y * 1000);
		if(d == Direction.EAST) return new Surrounding(getMapObject(x+1, y), d);
		if(d == Direction.WEST) return new Surrounding(getMapObject(x-1, y), d);
		if(d == Direction.NORTH) return new Surrounding(getMapObject(x, y-1), d);
		if(d == Direction.SOUTH) return new Surrounding(getMapObject(x, y+1), d);
		if(d == Direction.NORTHEAST) return new Surrounding(getMapObject(x+1, y-1), d);
		if(d == Direction.NORTHWEST) return new Surrounding(getMapObject(x-1, y-1), d);
		if(d == Direction.SOUTHEAST) return new Surrounding(getMapObject(x+1, y+1), d);
		if(d == Direction.SOUTHWEST) return new Surrounding(getMapObject(x-1, y+1), d);
		return null;
	}

	public MapObject createAnt() throws AntException {
		synchronized (this) {
			int newAntId = 0;

			List<Surrounding> surs = getSurroundings(nest.getId(), true);
			for(Surrounding s: surs) {
				GroundObject g = null;
				if(s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_GROUND) {
					g = (GroundObject)s.getMapObject();
				}
				//g.print();
				if(g != null && !g.isHasAnt()) {
					
					g.setAntWeightCarried(0);
					g.setHasAnt(true);
					g.setAtype(MapObject.ANT_TYPE_FORAGER);
					g.setAntEnergyAmount(nest.getFromFoodStored((MapObject.INIT_ANT_AMOUNT - 1)));
					map[nest.getId()] = nest.getData();
					map[g.getId()] = g.getData();
					newAntId = g.getId();
					//g.print();
					break;
				}
			}

			if(newAntId != 0) {
				return getMapObject(newAntId);
			} else {
				throw new AntException(AntException.ANTCREATEFAIL);
			}
		}
	}

	public void addFood(int x, int y) {
		synchronized (this) {
			MapObject o = getObject(x, y);
			if(o.getMapObjectType() == MapObject.MAP_TYPE_GROUND && !((GroundObject)o).isHasAnt()) {
				FoodObject f = new FoodObject(o.getId(), 0);
				f.setFoodWeight(0);
				f.setFoodAmount(100000);
				f.setFtype(MapObject.FOOD_TYPE_LIQUID);
				map[o.getId()] = f.getData();
			}
		}
	}

	public void decMones() {
		synchronized (this) {
			for(int i = 0; i < mapSize; i++) {
				MapObject m = createMapObject(i, map[i]);
				if(m.getMapObjectType() == MapObject.MAP_TYPE_GROUND) {
					GroundObject g = (GroundObject)m;
					if(g.decPheromones()) {
						map[i] = m.getData();
						//m.print();
					}
					
				}
			}
		}
	}

}
