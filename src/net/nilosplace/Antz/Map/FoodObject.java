package net.nilosplace.Antz.Map;

import java.awt.Color;

public class FoodObject extends MapObject {
	

	private int Ftype = 0;
	private long foodAmount = 0; // 2^30
	private long foodWeight = 0; // 2^30

	public FoodObject(int id, long data) {
		super(id, data);
		data >>= 2;
		init(data);
	}
	
	private void init(long data) {
		long ftype = data & 3;
		switch ((int)ftype) {
			case(FOOD_TYPE_LIQUID):
				Ftype = FOOD_TYPE_LIQUID;
				break;
			case(FOOD_TYPE_SOLID):
				Ftype = FOOD_TYPE_SOLID;
				break;
			default:
				Ftype = FOOD_TYPE_UNKNOWN;
		}
		data >>= 2;
		foodAmount = data & (MapObject.FOOD_MAX - 1);
		data >>= 30;
		foodWeight = data & (MapObject.FOOD_MAX - 1);
	}
	
	public long getFood() {
		long d = 0;
		d |= foodWeight;
		d <<= 30;
		d |= foodAmount;
		d <<= 2;
		d |= Ftype;
		d <<= 2;
		d |= MAP_TYPE_FOOD;
		data = d;
		return d;
	}
	
	public void updateData() {
		getFood();
	}

	public Color getColor() {
		return Color.GREEN;
	}
	
	public int getMapObjectType() { return MAP_TYPE_FOOD; }
	
	public void print() {
		System.out.println("-----------------------------Food----------------------------------");
		System.out.println("Id: " + id + " Food");
		String dataString = String.format("%64s", Long.toBinaryString(data)).replace(' ', '0');
		String type = dataString.substring(62,64); //2
		String ftype = dataString.substring(60,62); //2
		String famount = dataString.substring(30,60); //30
		String fweight = dataString.substring(0,30); //30
		System.out.println("|Food Weight                   |Food Amount                   |at|ot|");
		System.out.println("|" + fweight + "|" + famount + "|" +  ftype + "|" + type + "|");
		System.out.println("Ftype: " + Ftype);
		System.out.println("Food Amount: " + foodAmount);
		System.out.println("Food Weight: " + foodWeight);
		System.out.println("Data: " + dataString);
		System.out.println("----------------------------/Food----------------------------------");
	}
	
	public void takeFood(GroundObject mfrom) {
		long take_amount = 0;
		switch (Ftype) {
			case(FOOD_TYPE_LIQUID):
				take_amount = (8388607 - mfrom.getAntEnergyAmount());
				if(foodAmount >= take_amount) {
					foodAmount -= take_amount;
					mfrom.setAntEnergyAmount(mfrom.getAntEnergyAmount() + take_amount);
				} else {
					mfrom.setAntEnergyAmount(mfrom.getAntEnergyAmount() + foodAmount);
					foodAmount = 0;
				}
				break;
			case(FOOD_TYPE_SOLID):
				take_amount = (8388607 - mfrom.getAntWeightCarried());
				if(foodWeight >= take_amount) {
					foodWeight -= take_amount;
					mfrom.setAntWeightCarried(mfrom.getAntEnergyAmount() + take_amount);
				} else {
					mfrom.setAntWeightCarried(mfrom.getAntEnergyAmount() + foodWeight);
					foodWeight = 0;
				}
				break;
		}
	}

	public int getFtype() {
		return Ftype;
	}
	public void setFtype(int ftype) {
		Ftype = ftype;
	}
	public long getFoodAmount() {
		return foodAmount;
	}
	public void setFoodAmount(long foodAmount) {
		this.foodAmount = foodAmount;
	}
	public long getFoodWeight() {
		return foodWeight;
	}
	public void setFoodWeight(long foodWeight) {
		this.foodWeight = foodWeight;
	}

}
