package net.nilosplace.Antz.Map;

import net.nilosplace.Antz.Ants.Direction;

public class Surrounding {

	private MapObject mapObject;
	private Direction direction;
	
	public Surrounding(MapObject mapObject, Direction direction) {
		this.mapObject = mapObject;
		this.direction = direction;
	}
	public MapObject getMapObject() {
		return mapObject;
	}
	public void setMapObject(MapObject mapObject) {
		this.mapObject = mapObject;
	}
	public Direction getDirection() {
		return direction;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
}
