package net.nilosplace.Antz.Map;

public class AntException extends Exception {
	private static final long serialVersionUID = 5811706970737582749L;
	
	public static final int DIED = 0;
	public static final int BLOCKED = 1;
	public static final int ANTBLOCKED = 2;
	public static final int ANTCREATEFAIL = 3;
	public static final int NESTOUTOFFOOD = 4;
	public static final int UNKNOWN = 5;
	
	private int exceptionType = -1;

	public AntException(int exceptionType) {
		this.exceptionType = exceptionType;
	}
	public int getExceptionType() {
		return exceptionType;
	}
	public void setExceptionType(int exceptionType) {
		this.exceptionType = exceptionType;
	}
}
