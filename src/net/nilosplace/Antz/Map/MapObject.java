package net.nilosplace.Antz.Map;

import java.awt.Color;

public abstract class MapObject {

	protected int id;
	protected long data;
	
	public static final int MAP_TYPE_BLOCKAGE = 0;
	public static final int MAP_TYPE_NEST = 1;
	public static final int MAP_TYPE_FOOD = 2;
	public static final int MAP_TYPE_GROUND = 3;
	
	public static final int PHEROMONE_TYPE_NONE = 0;
	public static final int PHEROMONE_TYPE_TRAIL = 1;
	public static final int PHEROMONE_TYPE_REPELL = 2;
	public static final int PHEROMONE_TYPE_FORAGE = 3;
	
	public static final int ANT_TYPE_FORAGER = 0;
	public static final int ANT_TYPE_SOLDIER = 1;
	public static final int ANT_TYPE_UNKNOWN = 2;
	
	public static final int FOOD_TYPE_LIQUID = 0;
	public static final int FOOD_TYPE_SOLID = 1;
	public static final int FOOD_TYPE_UNKNOWN = 2;
	
	public static final int INIT_ANT_AMOUNT = 1500;
	public static final long ANT_MAX_AMOUNT = (long)Math.pow(2, 23);
	public static final long FOOD_MAX = (long)Math.pow(2, 30);
	public static final int ANT_MAX_COUNT = (int)Math.pow(2, 10);
	public static final int PHEROMONE_MAX = (int)Math.pow(2, 10);
	
	public MapObject(int id, long data) {
		this.id = id;
		this.data = data;
	}
	public MapObject(int id) {
		this.id = id;
	}
	public abstract Color getColor();
	public abstract void print();
	public abstract int getMapObjectType();
	public abstract void updateData();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getData() {
		updateData();
		return data;
	}
	public void setData(long data) {
		this.data = data;
	}
}
