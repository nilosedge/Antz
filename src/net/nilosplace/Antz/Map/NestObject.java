package net.nilosplace.Antz.Map;

import java.awt.Color;

public class NestObject extends MapObject {

	private long totalAntz = 0;
	private long foodStored = 0;
	
	public NestObject(int id, long data) {
		super(id, data);
		data >>= 2;
		init(data);
	}
	
	public NestObject(int id) {
		super(id);
	}
	
	private void init(long data) {
		totalAntz = data & 65535;
		data >>= 16;
		foodStored = data & (long)(Math.pow(2, 46) - 1);
	}
	
	public long getNest() {
		long d = 0;
		d |= foodStored;
		d <<= 16;
		d |= totalAntz;
		d <<= 2;
		d |= MAP_TYPE_NEST;
		data = d;
		return d;
	}

	public void updateData() {
		getNest();
	}

	public void print() {
		System.out.println("----------------------------Nest-----------------------------------");
		System.out.println("Id: " + id + " Nest");
		String dataString = String.format("%64s", Long.toBinaryString(data)).replace(' ', '0');
		String type = dataString.substring(62,64); //2
		String totalantz = dataString.substring(46,62); //16
		String foodstored = dataString.substring(0,46); //46
		System.out.println("|Food Stored                                   |Total Antz      |ot|");
		System.out.println("|" + foodstored + "|" + totalantz + "|" + type + "|");
		System.out.println("Total Ants: " + totalAntz);
		System.out.println("Food Stored: " + foodStored);
		System.out.println("Data: " + dataString);
		System.out.println("----------------------------/Nest----------------------------------");
	}

	public Color getColor() {
		return Color.CYAN;
	}
	
	public int getMapObjectType() { return MAP_TYPE_NEST; }
	
	public long getTotalAntz() {
		return totalAntz;
	}
	public void setTotalAntz(long totalAntz) {
		this.totalAntz = totalAntz;
	}
	
	
	public long addToFoodStored(long transfer_amount) {
		foodStored += transfer_amount;
		return transfer_amount;
	}

	public long getFromFoodStored(long transfer_amount) throws AntException {
		if(foodStored <= 0) throw new AntException(AntException.NESTOUTOFFOOD);
		if(foodStored < transfer_amount) {
			transfer_amount = foodStored;
			foodStored = 0;
		} else {
			foodStored -= transfer_amount;
		}
		return transfer_amount;
	}
}
