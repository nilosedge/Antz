package net.nilosplace.Antz.Map;

import java.awt.Color;

public class BlockageObject extends MapObject {
	
	public BlockageObject(int id, long data) {
		super(id, data);
	}

	public Color getColor() {
		return Color.BLUE;
	}

	public void print() {
		System.out.println("Id: " + id + " Blockage");
		System.out.println("Data: " + Long.toBinaryString(data));
	}
	
	public long getBlock() {
		int d = 0;
		data = d;
		return d;
	}
	
	public void updateData() {
		getBlock();
	}

	public int getMapObjectType() { return MAP_TYPE_BLOCKAGE; }

}
