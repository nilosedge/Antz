package net.nilosplace.Antz.Map;

import java.awt.Color;

public class GroundObject extends MapObject {
	
	private int Ptype = 0;
	private int Atype = 0;
	
	// 2^10
	private long pheromoneAmount = 0;
	// 2^1
	private boolean hasAnt = false;
	// 2^23
	private long antEnergyAmount = 0;
	// 2^23
	private long antWeightCarried = 0;
	
	private boolean debug = false;
	
	public GroundObject(int id, long data) {
		super(id, data);
		data >>= 2;
		init(data);
	}
	
	public GroundObject(int id) {
		super(id);
	}
	
	private void init(long data) {
		long ptype = data & 3;
		switch ((int)ptype) {
			case(PHEROMONE_TYPE_FORAGE):
				Ptype = PHEROMONE_TYPE_FORAGE;
				break;
			case(PHEROMONE_TYPE_TRAIL):
				Ptype = PHEROMONE_TYPE_TRAIL;
				break;
			case(PHEROMONE_TYPE_REPELL):
				Ptype = PHEROMONE_TYPE_REPELL;
				break;
			default:
				Ptype = PHEROMONE_TYPE_NONE;
		}
		data >>= 2;
		pheromoneAmount = data & (PHEROMONE_MAX - 1);
		data >>= 10;
		hasAnt = (data & 1) == 1;
		data >>= 1;
		if(hasAnt) initAnt(data);
	}
	
	private void initAnt(long data) {
		long atype = data & 3;
		switch ((int)atype) {
			case(ANT_TYPE_FORAGER):
				Atype = ANT_TYPE_FORAGER;
				break;
			case(ANT_TYPE_SOLDIER):
				Atype = ANT_TYPE_SOLDIER;
				break;
			default:
				Atype = ANT_TYPE_UNKNOWN;
		}
		data >>= 2;
		antEnergyAmount = data & (ANT_MAX_AMOUNT - 1);
		data >>= 23;
		antWeightCarried = data & (ANT_MAX_AMOUNT - 1);
	}
	
	public long getGround() {
		long d = 0;
		if(hasAnt) {
			d |= antWeightCarried;
			d <<= 23;
			d |= antEnergyAmount;
			d <<= 2;
			d |= Atype;
			d <<=1;
			d |= 1;
		}
		d <<= 10;
		d |= pheromoneAmount;
		d <<= 2;
		d |= Ptype;
		d <<= 2;
		d |= MAP_TYPE_GROUND;
		data = d;
		return d;
	}

	public void print() {
		System.out.println("-----------------------------Ground----------------------------------");
		System.out.println("Id: " + id + " Ground");
		String dataString = String.format("%64s", Long.toBinaryString(data)).replace(' ', '0');
		String type = dataString.substring(62,64); //2
		String ptype = dataString.substring(60,62); //2
		String pamount = dataString.substring(50,60); //10
		String hasant = dataString.substring(49,50); //1
		String atype = dataString.substring(47,49); //2
		String energy = dataString.substring(24,47); //23
		String weight = dataString.substring(1,24); //23
		System.out.println("|Weight                 |Energy                 |at|a|Pamount   |at|ot|");
		System.out.println("|" + weight + "|" + energy + "|" +  atype + "|" + hasant + "|" + pamount + "|" + ptype + "|" + type + "|");
		System.out.println("Ptype: " + Ptype);
		System.out.println("Pheromone Amount: " + pheromoneAmount);
		System.out.println("Has Ant: " + hasAnt);
		System.out.println("Ant Energy: " + antEnergyAmount);
		System.out.println("Ant Weight Carried: " + antWeightCarried);
		System.out.println("Data: " + dataString);
		System.out.println("----------------------------/Ground----------------------------------");
	}
	
	public void clearAntData() {
		antEnergyAmount = 0;
		antWeightCarried = 0;
		Atype = 0;
		hasAnt = false;
	}
	
	public void updateData() {
		getGround();
	}

	public void setAntData(GroundObject mfrom) {
		antEnergyAmount = mfrom.getAntEnergyAmount();
		antWeightCarried = mfrom.getAntWeightCarried();
		Atype = mfrom.getAtype();
		hasAnt = true;
	}
	
	public void useEnergy(int pheromoneType) {
		
		switch (pheromoneType) {
			case(PHEROMONE_TYPE_NONE):
				if(debug) System.out.println("Type: PHEROMONE_TYPE_NONE");
				break;
			case(PHEROMONE_TYPE_TRAIL):
				if(debug) System.out.println("Type: PHEROMONE_TYPE_TRAIL");
				if(Ptype == PHEROMONE_TYPE_TRAIL || Ptype == PHEROMONE_TYPE_NONE || Ptype == PHEROMONE_TYPE_FORAGE) {
					Ptype = PHEROMONE_TYPE_TRAIL;
					if(pheromoneAmount > 0) {
						pheromoneAmount++;
					} else {
						pheromoneAmount += 120;
					}
					if(pheromoneAmount > (PHEROMONE_MAX - 1)) pheromoneAmount = (PHEROMONE_MAX - 1);
				}
				antEnergyAmount--;
				break;
			case(PHEROMONE_TYPE_REPELL):
				if(debug) System.out.println("Type: PHEROMONE_TYPE_REPELL");
				pheromoneAmount = 1;
				Ptype = PHEROMONE_TYPE_REPELL;
				antEnergyAmount--;
				break;
			case(PHEROMONE_TYPE_FORAGE):
				if(debug) System.out.println("Type: PHEROMONE_TYPE_FORAGE");
				if(Ptype == PHEROMONE_TYPE_NONE) { 
					pheromoneAmount += 120;
					Ptype = PHEROMONE_TYPE_FORAGE;
					antEnergyAmount--;
				}
				break;
		}
		//if(antWeightCarried > 1000) {
		//	antEnergyAmount--;
		//}
		antEnergyAmount--;
		//print();
	}
	
	public void transferFood(NestObject mto) throws AntException {
		if(antEnergyAmount < INIT_ANT_AMOUNT) {
			long transfer_amount = INIT_ANT_AMOUNT - antEnergyAmount;
			antEnergyAmount += mto.getFromFoodStored(transfer_amount);
		} else {
			long transfer_amount = antEnergyAmount - INIT_ANT_AMOUNT;
			antEnergyAmount -= mto.addToFoodStored(transfer_amount);
		}
	}

	public Color getColor() {
		if(hasAnt) {
			return Color.RED;
		} else {
			if(pheromoneAmount > 0) {
				switch (Ptype) {
					case(PHEROMONE_TYPE_NONE):
						return Color.DARK_GRAY;
					case(PHEROMONE_TYPE_TRAIL):
						return Color.YELLOW;
					case(PHEROMONE_TYPE_FORAGE):
						return Color.ORANGE;
					case(PHEROMONE_TYPE_REPELL):
						return Color.PINK;
				}
			}
		}
		return Color.DARK_GRAY;
	}
	
	public int getMapObjectType() { return MAP_TYPE_GROUND; }

	public int getAtype() {
		return Atype;
	}
	public void setAtype(int atype) {
		Atype = atype;
	}
	public long getPheromoneAmount() {
		return pheromoneAmount;
	}
	public void setPheromoneAmount(long pheromoneAmount) {
		if(pheromoneAmount > (PHEROMONE_MAX - 1)) {
			this.pheromoneAmount = (PHEROMONE_MAX - 1);
		} else {
			this.pheromoneAmount = pheromoneAmount;
		}
	}
	public boolean isHasAnt() {
		return hasAnt;
	}
	public void setHasAnt(boolean hasAnt) {
		this.hasAnt = hasAnt;
	}
	public long getAntEnergyAmount() {
		return antEnergyAmount;
	}
	public void setAntEnergyAmount(long antEnergyAmount) {
		if(antEnergyAmount > 8388607) {
			this.antEnergyAmount = 8388607;
		} else {
			this.antEnergyAmount = antEnergyAmount;
		}
	}
	public long getAntWeightCarried() {
		return antWeightCarried;
	}
	public void setAntWeightCarried(long antWeightCarried) {
		if(antWeightCarried > 8388607) {
			this.antWeightCarried = 8388607;
		} else {
			this.antWeightCarried = antWeightCarried;
		}
	}

	public boolean decPheromones() {
		if(pheromoneAmount > 0) {
			pheromoneAmount--;
			return true;
		} else return false;
	}

	public int getPtype() {
		return Ptype;
	}
}
