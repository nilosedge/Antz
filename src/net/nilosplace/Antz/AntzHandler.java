package net.nilosplace.Antz;

import java.util.List;

import net.nilosplace.Antz.Ants.Ant;
import net.nilosplace.Antz.Ants.AntMove;
import net.nilosplace.Antz.Map.AntException;
import net.nilosplace.Antz.Map.GroundObject;
import net.nilosplace.Antz.Map.MapObject;
import net.nilosplace.Antz.Map.Mapper;
import net.nilosplace.Antz.Map.Surrounding;

public class AntzHandler extends Thread {

	List<Ant> antz;
	int numberOfAntz = 1000;
	Mapper map;
	
	public AntzHandler(Mapper map) {
		this.map = map;
		antz = map.getInitialAnts(this);
	}

	public void run() {
		for(Ant a: antz) {
			a.start();
		}
		
		while(true) {
			try {
				sleep(1000);
				map.decMones();
				//System.out.println("AntzHandler: decrease mones");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public AntMoveResponce moveAnt(AntMove move) throws AntException {
		return map.moveAnt(move);
	}
	
	public List<Surrounding> getSurroundings(int id, boolean ordered) {	
		return map.getSurroundings(id, ordered);
	}

	public MapObject getMapObject(int x, int y) {
		return map.getObject(x, y);
	}

	public void saveMap() {
		map.saveMap();
	}

	public void createAnt() throws AntException {
		MapObject o = map.createAnt();
		if(o != null) {
			Ant a = new Ant(o.getId(), this, (GroundObject)o);
			a.start();
		}
	}

	public void addFood(int x, int y) {
		map.addFood(x, y);
	}
}
