package net.nilosplace.Antz;

import net.nilosplace.Antz.Map.AntException;
import net.nilosplace.Antz.Map.MapObject;
import net.nilosplace.Antz.Map.NestObject;

public class Nester extends Thread {
	
	private AntzHandler ah;
	
	public Nester(AntzHandler ah) {
		this.ah = ah;
	}

	public void run() {
		while(true) {
			try {
				try {
					System.out.println("Attempting to create ant");
					ah.createAnt();
				} catch (AntException e) {
					System.out.println("Ant Create Failed: " + e.getExceptionType());
					//e.printStackTrace();
				}
				System.out.println("Ant Create Success");
				sleep(1000000);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
