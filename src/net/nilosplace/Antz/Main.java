package net.nilosplace.Antz;

import net.nilosplace.Antz.Map.Mapper;

public class Main {

	public static void main(String[] args) {
		Mapper map = new Mapper("map.data");
		
		AntzHandler ah = new AntzHandler(map);
		ah.start();
		Nester n = new Nester(ah);
		n.start();
		new DisplayWindow(ah);
		
	}
}
