package net.nilosplace.Antz;

import net.nilosplace.Antz.Map.GroundObject;

public class AntMoveResponce {

	private int newId;
	private long newAntEnergyAmount;
	private long newAntWeightCarried;
	
	public AntMoveResponce(GroundObject g) {
		this.newId = g.getId();
		this.newAntEnergyAmount = g.getAntEnergyAmount();
	}
	
	
	
	public int getNewId() {
		return newId;
	}
	public void setNewId(int newId) {
		this.newId = newId;
	}
	public long getNewAntEnergyAmount() {
		return newAntEnergyAmount;
	}
	public void setNewAntEnergyAmount(long newAntEnergyAmount) {
		this.newAntEnergyAmount = newAntEnergyAmount;
	}
	public long getNewAntWeightCarried() {
		return newAntWeightCarried;
	}
	public void setNewAntWeightCarried(long newAntWeightCarried) {
		this.newAntWeightCarried = newAntWeightCarried;
	}
}
