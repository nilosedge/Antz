package net.nilosplace.Antz;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import net.nilosplace.Antz.Map.GroundObject;
import net.nilosplace.Antz.Map.MapObject;

public class DisplayWindow extends JFrame {
	
	private static final long serialVersionUID = -8203440582994469232L;

	private final AntzHandler ah;
	private int w = 700;
	private int h = 700;
	
	// Scale
	//private double sc = 2;
	
	// Mouse Clicked
	private int mx = 0;
	private int my = 0;
	
	// Inner Window
	private double x = 0;
	private double y = 0;
	// Size
	private double xs = 500;
	private double ys = 500;

	private Image img = null;

	public DisplayWindow(final AntzHandler ah) {
		this.ah = ah;
		setSize(w, h);
		initListeners();
		addWindowListener(new WindowListener() {
			public void windowOpened(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowDeactivated(WindowEvent e) {}
			public void windowClosing(WindowEvent e) { /* ah.saveMap(); */ }
			public void windowClosed(WindowEvent e) {}
			public void windowActivated(WindowEvent e) {}
		});
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		img = createImage(w, h);
		while(true) {
			repaint();
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void initListeners() {
		addMouseMotionListener(new MouseMotionListener() {
			public void mouseMoved(MouseEvent e) {}
			public void mouseDragged(MouseEvent e) {
				if(e.getButton() == 1) {
					x -= (xs * ((e.getX() - mx) / (double)w));
					y -= (ys * ((e.getY() - my) / (double)h));
					mx = e.getX();
					my = e.getY();
					repaint();
				}
				if(e.getButton() == 3) {
					int cx = (int)((((double)e.getX() / (double)w) * xs) + x);
					int cy = (int)((((double)e.getY() / (double)h) * ys) + y);
					//System.out.println("Add food: " + cx + " : " + cy);
					ah.addFood(cx, cy);
					repaint();
				}
				
			}
		});
		
		addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				//System.out.println(e);
				int wheel = e.getWheelRotation();
				x -= (xs / 20) * wheel;
				xs += (xs / 10) * wheel;
				y -= (ys / 20) * wheel;
				ys += (ys / 10) * wheel;
				repaint();
			}
		});
		
		addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseClicked(MouseEvent e) {
				if(e.getButton() == 3) {
					int cx = (int)((((double)e.getX() / (double)w) * xs) + x);
					int cy = (int)((((double)e.getY() / (double)h) * ys) + y);
					//System.out.println("Add food: " + cx + " : " + cy);
					ah.addFood(cx, cy);
				}
			}
			public void mousePressed(MouseEvent e) {
				mx = e.getX();
				my = e.getY();
			}
		});
	}

	public void paint(Graphics g) {
		if(img != null) {
			Graphics g2 = img.getGraphics();
			g2.setColor(Color.black);
			g2.fillRect(0, 0, w, h);
			for(int xst = (int)x; xst < (x + xs); xst++) {
				for(int yst = (int)y; yst < (y + ys); yst++) {
					MapObject o = ah.getMapObject(xst, yst);
					if(o != null) {
						//if(o.getId() == 200201) {
						//	o.print();
						//	System.out.println(o.getColor());
						//}
						g2.setColor(o.getColor());
					} else {
						g2.setColor(Color.BLACK);
					}
					int box = (int)Math.ceil(((double)w / xs));
					int boy = (int)Math.ceil(((double)h / ys));
					if(box < 1) box = 1;
					if(boy < 1) boy = 1;
					
					g2.fillRect((int)((double)w * ((double)xst - x) / xs ), (int)((double)h * ((double)yst - y) / ys), (int)box, (int)boy);
				}
			}
		}
		g.drawImage(img, 0, 0, null);
	}
}
