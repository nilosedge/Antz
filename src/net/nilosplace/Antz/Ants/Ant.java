package net.nilosplace.Antz.Ants;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.nilosplace.Antz.AntMoveResponce;
import net.nilosplace.Antz.AntzHandler;
import net.nilosplace.Antz.Map.AntException;
import net.nilosplace.Antz.Map.GroundObject;
import net.nilosplace.Antz.Map.MapObject;
import net.nilosplace.Antz.Map.Surrounding;

public class Ant extends Thread {
	
	private AntzHandler ah;
	private boolean alive = false;
	private long antEnergyAmount = 0;
	private long antWeightCarried = 0;
	private int antType = MapObject.ANT_TYPE_UNKNOWN;
	private int id;
	private int stepCounter = 0;
	private int stepsFromLastP = 0;
	private AntState state = AntState.FORAGING;
	private AntState lastState = AntState.FORAGING;
	private Direction currentDirection = Direction.NORTH;
	private Direction fromDirection = Direction.NORTH;
	private boolean debug = true;
	private int findTrailCounter = 0;
	
	private Random rand;

	private enum AntState {
		FORAGING,
		FOOD_TO_NEST,
		NEST_TO_FOOD,
		GET_FOOD,
		GET_NEST,
		FOLLOWING_TRAIL
	}
	
	public Ant(int id, AntzHandler ah, GroundObject g) {
		this.ah = ah;
		this.id = id;
		antEnergyAmount = g.getAntEnergyAmount();
		antWeightCarried = g.getAntWeightCarried();
		antType = g.getAtype();
		alive = true;
		rand = new Random(System.currentTimeMillis());
	}
	
	public void run() {
		AntMove m = new AntMove();
		while(alive) {
			//System.out.println("run() started");
			try {
				//System.out.println("Ant: " + id + " State: " + state);
				List<Surrounding> surs = ah.getSurroundings(id, true);
				lastState = state;
				state = changeState(surs);
				m = getMove(surs);
				
				//System.out.println(m);
				AntMoveResponce res = ah.moveAnt(m);
				id = res.getNewId();
				antEnergyAmount = res.getNewAntEnergyAmount();
				antWeightCarried = res.getNewAntWeightCarried();
				stepCounter++;
				sleep(3000);
			} catch (AntException e) {
				
				if(e.getExceptionType() == AntException.DIED) {
					System.out.println("Ant: " + id + " died.");
					alive = false;
					return;
				}
				if(e.getExceptionType() == AntException.BLOCKED) {
					// Change direction and try again
					System.out.println("Ant: " + id + " is blocked");
				}
				if(e.getExceptionType() == AntException.ANTBLOCKED) {
					// Change direction and try again.
					// Look to go around and try again
					// Random left or right to go around
					
					if(rand.nextBoolean()) {
						currentDirection = change45DegreesRight(currentDirection);
					} else {
						currentDirection = change45DegreesLeft(currentDirection);
					}
					System.out.println("Ant: " + id + " is blocked by another ant");
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("run() finished");
		}
	}

	private AntMove getMove(List<Surrounding> surs) {
		if(state == AntState.GET_FOOD) { return getFoodMove(surs); }
		if(state == AntState.GET_NEST) { return getNestMove(surs); }
		if(state == AntState.FORAGING) { return getForagingMove(surs); }
		if(state == AntState.FOOD_TO_NEST) { return getFoodToNestMove(surs); }
		if(state == AntState.NEST_TO_FOOD) { return getNestToFoodMove(surs); }
		if(state == AntState.FOLLOWING_TRAIL) { return getFollowingTrailMove(surs); }
		return null;
	}

	private AntState changeState(List<Surrounding> surs) {
		
		/* Sample State Machine for the Ant
			FORAGING -> Finds Trail -> FOLLOWING_TRAIL

			FOOD_TO_NEST -> Finds Nest -> GET_NEST
			FOOD_TO_NEST -> Looses Trail -> FORAGING
			
			NEST_TO_FOOD -> Finds Food -> GET_FOOD 
			NEST_TO_FOOD -> Looses Trail -> FORAGING
			
			GET_FOOD -> Stomach is full -> FOOD_TO_NEST
			GET_NEST -> Stomach is empty -> NEST_TO_FOOD
			
			FOLLOWING_TRAIL -> Finds Food || Finds Nest -> NEST_TO_FOOD
			FOLLOWING_TRAIL -> Looses Trail -> FORAGING
			
			FORAGING -> Pick Random Move -> FORAGING
			FOOD_TO_NEST -> Keep on Trail -> FOOD_TO_NEST
			NEST_TO_FOOD -> Move down trail or Move to Food -> NEST_TO_FOOD
			FOLLOWING_TRAIL -> Try to find next mone -> FOLLOWING_TRAIL
		 */
		if(state == AntState.FORAGING && findsTrailCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " FORAGING -> FOLLOWING_TRAIL");
			return AntState.FOLLOWING_TRAIL;
		} else if(state == AntState.FORAGING && findsFoodCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " FORAGING -> GET_FOOD");
			return AntState.GET_FOOD;
		} else if(state == AntState.FOOD_TO_NEST && findsNestCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " FOOD_TO_NEST -> GET_NEST");
			return AntState.GET_NEST;
		} else if(state == AntState.FOOD_TO_NEST && loosesTrailCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " FOOD_TO_NEST -> FORAGING");
			return AntState.FORAGING;
		} else if(state == AntState.NEST_TO_FOOD && findsFoodCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " NEST_TO_FOOD -> GET_FOOD");
			return AntState.GET_FOOD;
		} else if(state == AntState.NEST_TO_FOOD && loosesTrailCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " NEST_TO_FOOD -> FORAGING");
			return AntState.FORAGING;
		} else if(state == AntState.GET_FOOD && stomachFullCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " GET_FOOD -> FOOD_TO_NEST");
			return AntState.FOOD_TO_NEST;
		} else if(state == AntState.GET_NEST && stomachEmptyCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " GET_NEST -> NEST_TO_FOOD");
			return AntState.NEST_TO_FOOD;
		} else if(state == AntState.FOLLOWING_TRAIL && findsFoodCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " FOLLOWING_TRAIL -> GET_FOOD");
			return AntState.GET_FOOD;
		} else if(state == AntState.FOLLOWING_TRAIL && findsNestCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " FOLLOWING_TRAIL -> GET_NEST");
			return AntState.GET_NEST;
		} else if(state == AntState.FOLLOWING_TRAIL && loosesTrailCondition(surs)) {
			if(debug) System.out.println("Ant: " + id + " FOLLOWING_TRAIL -> FORAGING");
			return AntState.FORAGING;
		}
		return state;
	}

	public void print() {
		System.out.println("---id: " + id);
		System.out.println("---alive: " + alive);
		System.out.println("---antEnergyAmount: " + antEnergyAmount);
		System.out.println("---antWeightCarried: " + antWeightCarried);
		System.out.println("---antType: " + antType);
	}




	

	
	// Conditions
	
	private boolean findsTrailCondition(List<Surrounding> surs) {
		for(Surrounding s: surs) {
			if(
				s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_GROUND &&
				((GroundObject)s.getMapObject()).getPtype() == MapObject.PHEROMONE_TYPE_TRAIL &&
				((GroundObject)s.getMapObject()).getPheromoneAmount() > 0 &&
				s.getDirection() != fromDirection
			) {
				return true;
			}
		}
		return false;
	}
	
	private boolean findsNestCondition(List<Surrounding> surs) {
		for(Surrounding s: surs) {
			if(s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_NEST) {
				return true;
			}
		}
		return false;
	}
	
	private boolean findsFoodCondition(List<Surrounding> surs) {
		for(Surrounding s: surs) {
			if(s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_FOOD) {
				return true;
			}
		}
		return false;
	}

	private boolean loosesTrailCondition(List<Surrounding> surs) {
		return stepsFromLastP > 2;
	}

	private boolean stomachFullCondition(List<Surrounding> surs) {
		//System.out.println("En: " + antEnergyAmount + " : " + MapObject.INIT_ANT_AMOUNT);
		return antEnergyAmount >= MapObject.INIT_ANT_AMOUNT;
	}

	private boolean stomachEmptyCondition(List<Surrounding> surs) {
		return antEnergyAmount <= MapObject.INIT_ANT_AMOUNT;
	}
	
	
	// Moves
	
	// TODO
	private AntMove getFollowingTrailMove(List<Surrounding> surs) {
		// Check for trail is current direction or left or right
		// Check for forge trail in current direction or left or right
		// if found follow it
		
		// That is not where I came from
		// has food choose the option with the lowest mone amount > 0
		// else choose the option with the highest amount

		List<Surrounding> temp = getSurroundingsInDirection(surs, currentDirection);
		Surrounding choose = getChooseNewDirection(surs, temp);
		
		if(choose != null) {
			stepsFromLastP = 0;
			currentDirection = choose.getDirection();
			return updateCountersGenerateMove(id, currentDirection, MapObject.PHEROMONE_TYPE_TRAIL);
		} else {
			stepsFromLastP++;
			return updateCountersGenerateMove(id, currentDirection, MapObject.PHEROMONE_TYPE_NONE);
		}

	}
	
	private Surrounding getChooseNewDirection(List<Surrounding> surs, List<Surrounding> temp) {
		Surrounding choose = null;
		if(antEnergyAmount > MapObject.INIT_ANT_AMOUNT) {
			Surrounding temp2 = getLowestPheromoneAmount(temp);
			if(temp2 == null) {
				choose = getLowestPheromoneAmount(surs);
			} else {
				choose = temp2;
			}
		} else {
			Surrounding temp2 = getHighestPheromoneAmount(temp);
			if(temp2 == null) {
				choose = getHighestPheromoneAmount(surs);
			} else {
				choose = temp2;
			}
		}
		return choose;
	}

	// TODO
	private AntMove getFoodToNestMove(List<Surrounding> surs) {

		Surrounding choose = getChooseNewDirection(surs, getSurroundingsInDirection(surs, currentDirection));
		
		if(choose == null && stepsFromLastP == 0) {
			stepsFromLastP++;
			return updateCountersGenerateMove(id, currentDirection, MapObject.PHEROMONE_TYPE_TRAIL);
		}
		
		if(choose != null && findTrailCounter == 0) {
			stepsFromLastP = 0;
			currentDirection = choose.getDirection();
			findTrailCounter = 0;
			return updateCountersGenerateMove(id, currentDirection, MapObject.PHEROMONE_TYPE_TRAIL);
		} else {
			List<Surrounding> temp = new ArrayList<Surrounding>();
			//System.out.println("LastState: " + lastState);
			//System.out.println("State: " + state);
			if(lastState != AntState.GET_FOOD) {
				switch(findTrailCounter) {
					case(0):
						System.out.println("Step 0: " + currentDirection);
						currentDirection = change45DegreesLeft(fromDirection);
						findTrailCounter++;
						break;
					case(1):
						System.out.println("Step 1: " + currentDirection);
						temp = getSurroundingsInDirection(surs, change90DegreesLeft(currentDirection));
						if(temp.size() > 0) {
							currentDirection = change90DegreesLeft(currentDirection);
							findTrailCounter = 0;
						} else {
							currentDirection = change45DegreesLeft(currentDirection);
							findTrailCounter++;
						} 
						break;
					case(2):
						System.out.println("Step 2: " + currentDirection);
						// Stay going in the current direction
						break;
					case(3):
						System.out.println("Step 3: " + currentDirection);
						temp = getSurroundingsInDirection(surs, change45DegreesRight(currentDirection));
						if(temp.size() > 0) {
							currentDirection = change45DegreesRight(currentDirection);
							findTrailCounter = 0;
						} else {
							findTrailCounter++;
						}
						break;
				}
			}
			//findTrailCounter++;
			stepsFromLastP++;
			return updateCountersGenerateMove(id, currentDirection, MapObject.PHEROMONE_TYPE_NONE);
		}
	}
	
	// TODO
	private AntMove getNestToFoodMove(List<Surrounding> surs) {
		return getFollowingTrailMove(surs);
	}
	
	
	private AntMove getNestMove(List<Surrounding> surs) {
		stepsFromLastP = 0;
		for(Surrounding s: surs) {
			if(s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_NEST) {
				currentDirection = reverseDirection(currentDirection);
				return updateCountersGenerateMove(id, s.getDirection(), MapObject.PHEROMONE_TYPE_NONE);
			}
		}
		return null;
	}

	private AntMove getFoodMove(List<Surrounding> surs) {
		stepsFromLastP = 0;
		for(Surrounding s: surs) {
			if(s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_FOOD) {
				currentDirection = reverseDirection(currentDirection);
				return updateCountersGenerateMove(id, s.getDirection(), MapObject.PHEROMONE_TYPE_NONE);
			}
		}
		return null;
	}
	
	private AntMove getForagingMove(List<Surrounding> surs) {
		double random = rand.nextDouble();
		
		if(random < .20 && random >= .10) {
			currentDirection = change45DegreesRight(currentDirection);
		}
		else if(random < .10) {
			currentDirection = change45DegreesLeft(currentDirection);
		}
		
		if(stepCounter % 2 == 0) {
			return updateCountersGenerateMove(id, currentDirection, MapObject.PHEROMONE_TYPE_FORAGE);
		} else {
			return updateCountersGenerateMove(id, currentDirection, MapObject.PHEROMONE_TYPE_NONE);
		}
	}
	
	// Helpers
	
	private List<Surrounding> getSurroundingsInDirection(List<Surrounding> surs, Direction dir) {
		List<Surrounding> temp = new ArrayList<Surrounding>();
		for(Surrounding s: surs) {
			if(
				s.getDirection() == dir &&
				s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_GROUND &&
				((GroundObject)s.getMapObject()).getPheromoneAmount() > 0 &&
				(((GroundObject)s.getMapObject()).getPtype() == MapObject.PHEROMONE_TYPE_TRAIL ||
				((GroundObject)s.getMapObject()).getPtype() == MapObject.PHEROMONE_TYPE_FORAGE)
				) {
				if(
					s.getDirection() == dir ||
					s.getDirection() == change45DegreesRight(dir) ||
					s.getDirection() == change45DegreesLeft(dir)
				) {
					temp.add(s);
				}
			}
		}
		return temp;
	}
	
	private Surrounding getLowestPheromoneAmount(List<Surrounding> surs) {
		long min = 1000000;
		Surrounding minSur = null;
		for(Surrounding s: surs) {
			if(s.getDirection() != fromDirection && s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_GROUND && ((GroundObject)s.getMapObject()).getPheromoneAmount() > 0) {
				if(((GroundObject)s.getMapObject()).getPheromoneAmount() < min) {
					min = ((GroundObject)s.getMapObject()).getPheromoneAmount();
					minSur = s;
				}
			}
		}
		return minSur;
	}
	
	private Surrounding getHighestPheromoneAmount(List<Surrounding> surs) {
		long max = 0;
		Surrounding maxSur = null;
		for(Surrounding s: surs) {
			if(s.getDirection() != fromDirection && s.getMapObject().getMapObjectType() == MapObject.MAP_TYPE_GROUND && ((GroundObject)s.getMapObject()).getPheromoneAmount() > 0) {
				if(((GroundObject)s.getMapObject()).getPheromoneAmount() > max) {
					max = ((GroundObject)s.getMapObject()).getPheromoneAmount();
					maxSur = s;
				}
			}
		}
		return maxSur;
	}
	
	private Direction reverseDirection(Direction d) {
		if(d == Direction.NORTH)     return Direction.SOUTH;
		if(d == Direction.NORTHEAST) return Direction.SOUTHWEST;
		if(d == Direction.EAST)      return Direction.WEST;
		if(d == Direction.SOUTHEAST) return Direction.NORTHWEST;
		if(d == Direction.SOUTH)     return Direction.NORTH;
		if(d == Direction.SOUTHWEST) return Direction.NORTHEAST;
		if(d == Direction.WEST)      return Direction.EAST;
		if(d == Direction.NORTHWEST) return Direction.SOUTHEAST;
		return Direction.NORTH;
	}
	
	private Direction change90DegreesLeft(Direction dir) {
		if(dir == Direction.NORTH)     return Direction.WEST;
		if(dir == Direction.NORTHWEST) return Direction.SOUTHWEST;
		if(dir == Direction.WEST)      return Direction.SOUTH;
		if(dir == Direction.SOUTHWEST) return Direction.SOUTHEAST;
		if(dir == Direction.SOUTH)     return Direction.EAST;
		if(dir == Direction.SOUTHEAST) return Direction.NORTHEAST;
		if(dir == Direction.EAST)      return Direction.NORTH;
		if(dir == Direction.NORTHEAST) return Direction.NORTHWEST;
		return Direction.SOUTH;
	}
	
	private Direction change90DegreesRight(Direction dir) {
		if(dir == Direction.NORTH)     return Direction.EAST;
		if(dir == Direction.NORTHWEST) return Direction.NORTHEAST;
		if(dir == Direction.WEST)      return Direction.NORTH;
		if(dir == Direction.SOUTHWEST) return Direction.NORTHWEST;
		if(dir == Direction.SOUTH)     return Direction.WEST;
		if(dir == Direction.SOUTHEAST) return Direction.SOUTHWEST;
		if(dir == Direction.EAST)      return Direction.SOUTH;
		if(dir == Direction.NORTHEAST) return Direction.SOUTHEAST;
		return Direction.SOUTH;
	}

	private Direction change45DegreesLeft(Direction dir) {
		if(dir == Direction.NORTH)     return Direction.NORTHWEST;
		if(dir == Direction.NORTHWEST) return Direction.WEST;
		if(dir == Direction.WEST)      return Direction.SOUTHWEST;
		if(dir == Direction.SOUTHWEST) return Direction.SOUTH;
		if(dir == Direction.SOUTH)     return Direction.SOUTHEAST;
		if(dir == Direction.SOUTHEAST) return Direction.EAST;
		if(dir == Direction.EAST)      return Direction.NORTHEAST;
		if(dir == Direction.NORTHEAST) return Direction.NORTH;
		return Direction.SOUTH;
	}

	private Direction change45DegreesRight(Direction dir) {
		if(dir == Direction.NORTH)     return Direction.NORTHEAST;
		if(dir == Direction.NORTHEAST) return Direction.EAST;
		if(dir == Direction.EAST)      return Direction.SOUTHEAST;
		if(dir == Direction.SOUTHEAST) return Direction.SOUTH;
		if(dir == Direction.SOUTH)     return Direction.SOUTHWEST;
		if(dir == Direction.SOUTHWEST) return Direction.WEST;
		if(dir == Direction.WEST)      return Direction.NORTHWEST;
		if(dir == Direction.NORTHWEST) return Direction.NORTH;
		return Direction.NORTH;
	}
	
	private AntMove updateCountersGenerateMove(int id, Direction d, int pheromoneType) {
		fromDirection = reverseDirection(d);
		return new AntMove(id, d, pheromoneType);
	}
	
}
