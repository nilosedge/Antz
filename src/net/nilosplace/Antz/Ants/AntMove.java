package net.nilosplace.Antz.Ants;

import net.nilosplace.Antz.Map.MapObject;


public class AntMove {
	private int id;
	private Direction direction;
	private int pheromoneType;
	
	public AntMove(int id, Direction d, int pheromoneType) {
		this.id = id;
		this.direction = d;
		this.pheromoneType = pheromoneType;
	}
	public AntMove() {
		id = 0;
		direction = Direction.NORTH;
		pheromoneType = MapObject.PHEROMONE_TYPE_NONE;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Direction getDirection() {
		return direction;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	public int getPheromoneType() {
		return pheromoneType;
	}
	public void setPheromoneType(int pheromoneType) {
		this.pheromoneType = pheromoneType;
	}
	
	@Override
	public String toString() {
		return "AntMove [id=" + id + ", direction=" + direction + ", pheromoneType=" + pheromoneType + "]";
	}
	
	
}
